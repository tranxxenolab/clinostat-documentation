Documentation for the [Open Source Clinostat](https://tranxxenolab.net/projects/clinostat/)

# Repositories

I have split out the separate components to individual repositories on Gitlab:

* [Designs for clinostat vessel, frame, and boxes](https://gitlab.com/tranxxenolab/clinostat-design)
* [Schematics and layout for clinostat control board](https://gitlab.com/tranxxenolab/clinostat-control-board)
* [Software for clinostat control board](https://gitlab.com/tranxxenolab/clinostat)

# Support

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance, Kristijan Tkalec for his expert scientific assistance, and Meta Petrič for her hands-on assistance. Special thanks to Špela Petrič.

Created during a [Biofriction](https://biofriction.org/) residency.

Produced by [Galerija Kapelica](http://kapelica.org/)/[Zavod Kersnikova](https://kersnikova.org/) in Ljubljana, Slovenia.

Biofriction is supported by the European Commission--Creative Europe.
