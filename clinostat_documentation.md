---
title: Clinostat Documentation
author: Adriana Knouf
---

<!--

pandoc -F pandoc-crossref -F pandoc-citeproc --pdf-engine=xelatex --template ask --bibliography ~/Nextcloud/bibliography/PersonalBibliographicDatabase_JabRef.bib --csl chicago-fullnote-bibliography-with-ibid.csl clinostat_documentation.md -o clinostat_documentation.pdf

Creating an ICML file for InDesign:

pandoc -s -F pandoc-crossref -F pandoc-citeproc --bibliography ~/Nextcloud/bibliography/PersonalBibliographicDatabase_JabRef.bib --csl chicago-fullnote-bibliography-with-ibid.csl clinostat_documentation.md -o clinostat_documentation.icml

https://github.com/jgm/pandoc/wiki/Importing-Markdown-in-InDesign

-->

# Rationale

Sending biological projects to space is expensive and challenging, even with enormous resources. For artists, it's nigh impossible without specialised relationships with governmental or commercial entities, meaning that many interesting possibilities for experimentation are limited to those with lots of capital (monetary and social). 

But it's also possible to simulate certain aspects of the microgravity--or "weightless"--environment here on earth. One of the most common ways for research involving mammalian cells is the ["rotating wall vessel"](https://lsda.jsc.nasa.gov/Hardware/hardw/1248) (RWV, also sometimes called the "fast rotating clinostat" or "clinostat" for short), originally developed by NASA in the 1980s. These devices have also been used in recent decades for growing 3D tissue cultures (and have been seen within the bioart context, most notably with the Tissue Culture and Art Project's [*Semi-living Worry Dolls*](https://tcaproject.net/portfolio/worry-dolls/) (2001)). The theory of the devices is relatively simple in concept, but complicated in its details[@Briegleb1992a]. The idea is that cells grow in medium embedded in a liquid of high viscosity, all within a device that is rotating at a constant speed. At a certain number of revolutions per minute, the cells in the growth medium will not be traveling around the circumference of the vessel, but will rather appear as if they are "floating", even as the cells inside are constantly in a state of rotation. Taking a time average of their gravitational vector, this will average out to zero--thus simulated weightlessness. At no point are the cells actually in a state of free fall, but rather it is the *time average* that is zero. But various experiments have shown that this is a relatively good and robust means of simulating microgravity[@Cogoli1992a;@Klaus2001a;@Hemmersbach2006a;@Grimm2014a].

Nevertheless, the costs of commercial clinostats can be prohibitive. A reusable, autoclavable 10mL RWV from a supplier such as [Synthecon](https://synthecon.com/pages/autoclavable_vessel_culture_systems_synthecon_23.asp) can run upwards of €1700--and this is just for the vessel alone, and not for the control box and associated hardware. Again, this is cost-prohibitive for artists or biohackers on limited budgets.

Because of these reasons, we decided to create an open version of the clinostat within the ethos of transhackfeminism--namely, making these research tools free and accessible for others to further the queering of science and bioart, especially within the outer space context. This work was one of the main outcomes of my [Biofriction](https://biofriction.org/) residency, ["Xenological Entanglements. 001: Eromatase"](https://tranxxenolab.net/projects/eromatase/), produced by [Kersnikova Institute/Kapelica Gallery](https://kersnikova.org/) in the fall of the pandemic year 2020.

The elements of this open design are:

* Reverse-engineered vessel for growing cells, made out of glass and teflon (in our version);
* Custom 3D-printed frame for mounting the vessel and rotating it;
* Electronics hardware to control the vessel's rotation and provide some visual indicators of the machine's properties, installed in two external control boxes; and,
* Software for controlling the rotational speed of the vessel, providing an interface for various sensors, and allowing for future expansion (including logging of data/sensor readings, remote monitoring, etc.).

All of these elements are integrated together and designed in such a way that the frame and vessel can be placed inside most incubators, with the control boxes remaining outside.

The following git repositories include all of the files necessary to build this project:

* [clinostat design files](https://gitlab.com/tranxxenolab/clinostat-design): the design files for the vessel, frame, and enclosing boxes for the electronics
* [clinostat control board](https://gitlab.com/tranxxenolab/clinostat-control-board): schematic, layout, and Gerber files for the clinostat control PCB
* [clinostat](https://gitlab.com/tranxxenolab/clinostat): source code for the firmware for the ESP32, as a PlatformIO project
* [clinostat documentation](https://gitlab.com/tranxxenolab/clinostat-documentation): the files for producing this document


![Process of clinostat development](media/clinostat_process_1500px.png){#fig:process} 

# Process


While the principle of the RWV is quite simple, assembling everything into a coherently functioning system is not an easy task. We iterated over a number of different options (Figure -@fig:process), especially playing around with reducing the gear ratio so that a stepper motor, which prefers to rotate rather rapidly, can move the shaft that holds the vessel, which should move relatively slower.

Additionally, we wanted to have some real-time feedback for the acceleration of the vessel, as well as ambient temperature around it. This necessitated the development of a slip ring system, which still has some work to be done on it before it is properly functioning.

# Components

## Vessel

![Completed rotating wall vessel](media/clinostat/1500px/20121116-DSCF5207.png){#fig:completed_vessel}


![Underside of the vessel bottom](media/clinostat/1500px/20121116-DSCF5224.png){#fig:vessel_bottom_underside}

![Top of the vessel bottom](media/clinostat/1500px/20121116-DSCF5230.png){#fig:vessel_bottom_top}

![Glass top](media/clinostat/1500px/20121116-DSCF5235.png){#fig:vessel_top}

![Teflon plug and valves](media/clinostat/1500px/20121116-DSCF5211.png){#fig:teflon_plug}

Lovrenc Košenina modeled the vessel based on existing RWV designs. It's somewhat unclear what exact materials are used in the construction of the commercial vessels, given their proprietary nature. But two of the main challenges in this design were finding materials that could be a) machined easily; and b) were autoclavable. Cheapness of the materials was also a factor :)

After a lot of discussion between ourselves and Kristijan Tkalec, we decided to use glass for the top (rather than some kind of polycarbonate probably used in the commercial vessels) and teflon for the bottom as well as the plugs. It's also of course important to ensure that the seals are tight between the top and the bottom, as well as between the plugs and the top, thus having O-rings that allow for this seal, but also for easy removal, was also a factor.

It's unfortunately not easy to precisely specify the exact O-rings used, given variations in machining. So the ones listed in the parts list should be understood as starting points for experimentation.

The top of the values were designed to be compatible with Luer lock components, such as valves or syringes, but also to be easily machinable by hand.

## Frame

![Completely assembled frame with vessel installed](media/clinostat/1500px/20121117-DSCF5266.png){#fig:frame_vessel}

![Clinostat schematic view (image courtesy of Lovrenc Košenina)](media/clinostat_back_side.jpg){#fig:clinostat_back_side}

![Clinostat schematic exploded view (image courtesy of Lovrenc Košenina)](media/clinostat_exploded.jpg){#fig:clinostat_exploded}

Lovrenc Košenina designed the frame to hold the motor for spinning the vessel, a pulley system for changing ratios, and a wheel+comb to hold a slip-ring for an accelerometer and temperature sensor--which is not presently functioning as desired right now. More on this below.


## Control Board and Code

![3D render of clinostat control board](media/clinostat_esp32_faceOn.png){#fig:render}

The control board (Figure -@fig:render) is based around an ESP32 development platform. There are many different versions of these boards that one can purchase, each with a different pinout. As a result, if you use a version that is not exactly the same as the one we used, it will not be a drop-in replacement, and will thus require modifications to the circuit board design. We used the ["ESP32 NodeMCU Module WLAN WiFi Development Board with CP2102"](https://www.az-delivery.de/en/products/esp32-developmentboard) from [AZ Delivery](https://www.az-delivery.de). This board is cheap, has proven to be robust for our purposes, and is easily available within Europe directly from the supplier or from Amazon.de.

![Clinostat under development on breadboard](media/clinostat_breadboard.jpg){#fig:development}

The purpose of the board is to have an easy way to interconnect all of the various components together: from the power converter circuitry, to a socket for the ESP32 board, to pins for jumper cables to external peripherals. All of this could have easily been wired up on a breadboard (and was done so during the development, see Figure -@fig:development), but in order to make things more robust we developed the PCB. This has proven to be very useful. The [schematic and board can be found on Gitlab](https://gitlab.com/tranxxenolab/clinostat-control-board).

Power for the clinostat is 24V DC. In various configurations we have used AC/DC converter modules, benchtop power supply, or wall warts. The design detailed here uses a Mean Well AC/DC converter module for ease of connecting to mains power. **Do not build this project as we have designed it if you have concerns about working with mains power!!!** If you have these concerns, then source an appropriate 24V wall wart (such as [this](https://smile.amazon.de/-/en/AveyLum-Universal-Adapter-Transformer-Printer/dp/B0721MKCZJ/ref=sr_1_6?dchild=1&keywords=24+v+netzteil&qid=1622731456&sr=8-6)). We won't detail how to wire up the plug for mains power; the instructions below include a link to an Internet site that details what needs to be done.

![Annotation of areas of clinostat board](media/clinostat_esp32_annotatedBoard.png)
    
Each output pin is clearly labeled, allowing for easy interconnections with the exterior components. Given different pinouts for the components, however, one might need to make custom jumper cables (i.e., wires might need to twist over each other).

These are what the various output pins mean:

* **MOTOR_CTL**: Direction and Step lines that go to the motor driver board
* **SPEED_POT**: Potentiometer to control the motor speed
* **MOTOR_EN**: SPDT switch to enable the motor to be turned on or off
* **ACCEL_I2C**: I2C and power for accelerometer board
* **OLED**: I2C and power for OLED screen
* **MISC1_I2C**: Extra I2C and power connection for a miscellaneous peripheral
* **SCREEN_SPI**: SPI and power for an external screen--although we have had problems getting this to work properly
* **J10** (although it should be labeled as **PWM**): PWM and power connection to PWM board--this is only used for the artwork *Saccular Fount*, but could be used to control anything, such as a peristaltic pump, that needs PWM control
* **3V3_RAIL**: Pins for 3.3V power
* **5V_RAIL**: Pins for 5V power

## Hardware

![Assembled control boxes. Power is in the left box, the remaining components are in the right box](media/clinostat/1500px/20121117-DSCF5262.png){#fig:control_boxes}

All of the electrical components are housed within two custom, laser-cut boxes (Figure -@fig:control_boxes). These boxes house the AC/DC converter, microstepping motor control driver, clinostat control board, and external peripherals like the potentiometer, switch, and OLED screen. Power from the left box is transferred to the right box through a two-wire pair terminated with a standard 5.5mm plug.  Connections to the frame (which will sit inside the incubator during actual use) come from a flat ribbon cable that exits the right box. These wires carry power for the motor and power and data for the accelerometer/temperature board.


# Parts

## Vessel

* Machined glass top
* Machined teflon bottom
* Machined teflon plug
* 2 machined teflon valves
* ~48mm O-ring to go between the top and bottom
* 6 ~5-6mm O-rings for the plug and valves (2 each)
* 6 M4x32 (or M4x30) hex screws with a 20mm long shank (to hold the glass top to the bottom)

## Frame

* Two vertical supports (`VerticalStand.stl`)
* One plexiglass bottom plate (`Stand_BottomPlate.dxf`)
* One small wheel with sprockets for the motor (`SmallWheel.stl`)
* One large wheel with sprockets (`BigWheel.stl`)
* M16x120 bolt with long shank (at least 80mm long)
* 2 M16 ball bearings (check to ensure it will fit in this design, if not, some changes to the frame arms might be needed)
* M16 nut
* NEMA 17 motor (such as [this](https://smile.amazon.de/-/en/Stepperonline-Stepper-1-8deg-connector-printer/dp/B00PNEQKC0/ref=sr_1_5?dchild=1&keywords=nema+17+motor&qid=1627480266&sr=8-5))
* M300 GT2 timing belt
* 4 M3x8 screws (to secure the motor)
* 4 M3x10 screws (to secure the vertical supports)
* DuPont pins and sockets
* (Optional) One slip-ring wheel
* (Optional) One comb support made of two parts for articulation
* (Optional) Aluminum strips
* (Optional) M3x15 screw and nut
* (Optional) 2 M3x6 screws
* (Optional) Copper tape
* (Optional) Hook-up wire

## Control Boxes

* 24V AC/DC converter (such as [this](https://si.farnell.com/mean-well/lrs-150-24/power-supply-ac-dc-24v-6-5a/dp/2815975))
* Fused IEC plug (such as [this](https://www.amazon.de/-/en/gp/product/B07T5D39R3/ref=ppx_od_dt_b_asin_title_s01?ie=UTF8&psc=1)) 
* Microstepping motor driver (such as [this](https://www.amazon.de/-/en/Topdirect-TB6600-Stepper-Stepper-Controller-Printer/dp/B0711J1K66/ref=sr_1_35?dchild=1&keywords=microstepping+driver&qid=1622813724&sr=8-35))
* [Custom control PCB](https://gitlab.com/tranxxenolab/clinostat-control-board)
* AZ Delivery ESP32-WROOM-32 (such as [this](https://www.amazon.de/gp/product/B074RGW2VQ/ref=ppx_yo_dt_b_asin_title_o07_s00?ie=UTF8&psc=1)). **NOTE**: it is important to choose the same version of ESP32, as the custom PCB is designed with this pinout in mind
* I2C OLED screen (such as [this](https://www.az-delivery.de/en/products/0-96zolldisplay?_pos=1&_sid=9ab907aa4&_ss=r))
* XL4015-based DC-DC converter (such as [this](https://www.amazon.de/gp/product/B084BVKGDK/ref=ppx_yo_dt_b_asin_title_o06_s01?ie=UTF8&psc=1))
* MPU-6050 accelerometer board (such as [this](https://www.amazon.de/-/en/dp/B09962TSDC/ref=sr_1_18?dchild=1&keywords=beschleunigungsmesser+board&qid=1627048799&sr=8-18))
* LD1117V33 regulator (TO-220 package, 1x)
* 100 ohm resistor (2x)
* 10k ohm resistor (2x)
* 10uF polarized electrolytic capacitor (2x)
* Green LED (3mm, 1x)
* Red LED (3mm, 1x)
* 2N7000 transistor (TO-92 package, 2x)
* SPDT switch
* Potentiometer (20k)
* 5.5x2.1 plug and receptacle
* Header pins (35)
* DuPont plugs and sockets (2-pin socket (1x), 3-pin socket (2x), 4-pin socket (5x), 4-pin plug (3x))
* 4 M3x8 nylon screws and nuts
* Hookup wire
* Heat shrink tubing
* Micro USB cable
* Ribbon cable (at least 8 conductor)
* Laser cut MDF sheets for boxes
* 48 M3x6 bolts and nuts (to hold the boxes together)

# Assembly Instructions

For photo documentation of these assembly steps, see the section "Assembly Photos".

## Vessel

![Vessel parts](media/clinostat/1500px/20121116-DSCF5248.png){#fig:vessel_parts}

![Vessel bottom with O ring](media/clinostat/1500px/20121116-DSCF5251.png){#fig:vessel_bottom_oring}

![Vessel with glass top, partially screwed together](media/clinostat/1500px/20121116-DSCF5258.png){#fig:vessel_top_screwed}


1. Have machined the glass top, teflon bottom, and teflon plugs using the [provided schematics](https://gitlab.com/tranxxenolab/clinostat-design/-/tree/master/vessel).
1. Slip the O rings on the plug and valves.
1. Place the large O ring on the teflon bottom.
1. Carefully place the glass lid on top of the bottom + O ring, and adjust the glass top so the O ring fits snugly in its slot.
1. Insert and *carefully* tighten the M4 screws so as to not break the glass lid. *Do not overtighten.* Tighten the screws evenly (always opposite pairs of screws).
1. Insert the plug and valves.
1. Screw stoppers on the valves; get these from extra syringes you might have sitting around :)

## Frame

### 3D Printing and Laser Cutting

3D print the parts in [the repository](https://gitlab.com/tranxxenolab/clinostat-design/-/tree/master/frame). The files you need to print are `BigWheel.stl`, `SmallWheel.stl` and two of `VerticalStand.stl`. Use PLA or PETG. If you want to try and build the slip ring system, then print the remaining STL files.

Laser cut the base out of acrylic. Use the file `Stand_BottomPlate.dxf`.

### Stepper Motor Preparation

1. Cut off the original connector.
1. Cut an appropriate length of heat shrink tubing to secure the wires, slide along the wires, and heat up to induce shrinking.
1. Crimp new DuPont pins and sheath at the end of the wires.

### Frame Assembly

1. Press the ball bearings into the holes at the top of the two vertical supports. **NOTE**: This is a press-fit installation, and is designed to be extremely tight. Once the ball bearings are secured, it will be nearly impossible to remove them without destroying the supports!
1. Screw the motor onto one of the vertical supports through the holes provided. Use the 4 M3x8 screws. Orient the motor near the top of the slot. Be sure to not overtighten the screws, as you will be sliding the motor down in a later step.
1. Place the smaller wheel with sprockets on the end of the motor shaft.
1. Attach this assembled support at the end of the acrylic plate using two of the M3x10 screws. Ensure that the motor shaft is pointing down the length of the base plate.
1. Slide the M16 bolt through the ball bearings from the outside.
1. Slide the large wheel with sprockets along the M16 bolt. It's meant to be tight :) Use a small set screw if necessary to hold the wheel even more securely.
1. Place M300 GT2 belt around both the large and small wheels.
1. Adjust the motor downwards to ensure that the belt fits snugly over the two wheels. Tighten the screws securing the motor if desired.
1. (Optional) Slide on the assembled slip ring wheel. (See more detailed information about construction below.)
1. (Optional) Secure the comb with aluminum wires to the acrylic plate using 2 M3x10 screws.
1. Slide the M16 bolt through the ball bearings of the second vertical support.Secure on the acrylic plate using the remaining two M3x10 screws. 
1. Screw the M16 bolt snugly on the outside of the second support, but not too tightly so as to cause the support to lean.

Your frame is now complete, sans vessel!

### Slip Ring Construction

![Slip ring, showing copper tape around circumference](media/clinostat/1500px/20121117-DSCF5328.png){#fig:slip_ring_copper}

![Slip ring, showing inside connections](media/clinostat/1500px/20121117-DSCF5326.png){#fig:slip_ring_inside}

This part of the clinostat is not completely finished yet. The idea is to use a [slip ring](https://en.wikipedia.org/wiki/Slip_ring) design to transfer the electrical signals from the rotating shaft to a stationary comb on the base so as to receive information from the acceleration and temperature sensors. We have not been able to properly engineer this part yet. As you can see in the images (Figures -@fig:slip_ring_copper and -@fig:slip_ring_inside), the idea was to mount the sensor within a large wheel and tie the data and power lines into copper tape that goes around the wheel. Then, a stationary aluminum "comb" would make contact with the wheel and transfer the signals to wires that would then run to the motor control board. We have had problems making the connections stable and would appreciate suggestions from others about how to solve this problem!

The 3D-printed comb arm for the slip ring is attached to the base using 2 M3x6 screws. The concept was to have small strips of aluminum then screwed into the comb arm, which would then have hook-up wire wrapped around the screws for transferring the data signals to the ribbon cable. In the end, this solution has proven to be rather finicky and needs to be re-engineered.

### Hardware Assembly

Laser cut the boxes out of 0.5cm thick MDF, using the file `Boxes.dxf` in [this repository](https://gitlab.com/tranxxenolab/clinostat-design/-/tree/master/box). One box will be for the power converter and power wires, the other will be for the control board, stepper motor driver, etc.

#### Wiring the power converter box

1. Secure the AC/DC converter to the bottom panel of the box using M3 screws.
1. Install the IEC plug in the box.
1. Wire the IEC plug as usual. This will probably require some crimping and properly-rated spade connectors. **NOTE: DO THIS ONLY IF YOU KNOW WHAT YOU ARE DOING! INCORRECT INSTALLATION CAN RESULT IN SERIOUS INJURY OR DEATH DUE TO ELECTROCUTION!!!** For more information on how to do this see, for example, <https://www.instructables.com/Wire-Up-a-Fused-AC-Male-Power-Socket/>.
1. Solder a 5.5x2.1mm plug to one end of a pair of wires. Connect the ends into the positive and negative DC output of the AC/DC converter through the hole in the back of the power converter box.
1. Assemble the remaining panels of the box using the M3 screws and nuts.

#### Preparation for the main electronics box

1. Solder wires to the potentiometer and the SPDT switch that are long enough to reach the corresponding points on the control PCB. Crimp Dupont sockets to the ends of the wires.
1. Make a wire with crimped Dupont sockets at both ends that is long enough to go from the OLED screen to the control PCB. Attach one end to the OLED screen.
1. Solder two long wires to the 5.5x2.1mm socket. At the end of each wire, solder two more wires to it, long enough to reach both the stepper driver board and the clinostat PCB. Protect the soldered sections with heat shrink.
1. Take two strands of hookup wire, crimp Dupont sockets to one end. This will be for the DIR and STEP connections to the stepper driver board from the control PCB.
1. Take four strands of hookup wire, crimp Dupont pins to one end. This is for the four conductors for the stepper motor, which will then connect with the ribbon cable (made in the next step).
1. Take a long length of 8-conductor ribbon cable. (This should be long enough to reach from the electronics box into the incubator you will be using.) At both ends of the cable, split the 8-conductor cable into two sets of four wires. Crimp Dupont sockets to each set of four wires.
1. Assemble the clinostat control board.

#### Assembling the clinostat control board

1. Adjust the voltage for the XL4015 step-down converter board. Apply 24V input to the board. Adjust the voltage until the output reads 5V.
1. Upload the compiled firmware to the ESP32 board.
1. Solder header sockets on the board for all of the pins for the ESP32.
1. Solder header pins for all of the connectors on the board.
1. Solder the through-whole components (resistors, capacitors, voltage regulator, LEDs, etc).
1. Solder wires for the output of the step-down converter board.
1. Attach the step-down converter board to the control board using nylon screws. **Ensure it is oriented correctly!**
1. Install the ESP32 into the pin sockets. **Ensure it is oriented correctly and the pinout matches the board!**

#### Wiring and installing components in the main electronics box

1. Install the 5.5x2.1mm receptacle and soldered wires on the back panel of the box.
1. Set the microstepping value to 800 and current limit to ~1.7A max on the stepper motor driver board. Install the stepper motor driver board on the left side panel of the box.
1. Install the motor control board on the bottom panel of the box.
1. Install the OLED screen, SPDT switch, and potentiometer to the top panel of the box. 
1. Wire up the four wire conductors that go from the stepper driver board to the ribbon cable (and eventually the motor).
1. Wire up the two wire conductors that go from the stepper driver board to the clinostat control board.
1. Wire one end each of the power and ground wires (from the 5.5x2.1mm socket) to the stepper driver board.
1. Wire jumpers from the ground on the stepper driver board to appropriate  screw terminals on the board.
1. Connect the remaining power and ground wires to the input for the step-down converter board.
1. Connect the output wires for the step-down converter board (soldered in the previous step) to the output terminal block.
1. Attach the Dupont connectors for all of the remaining peripherals.
1. Feed the ribbon cable outside of the box.
1. Connect the micro-USB cable to the ESP32 and feed the other end outside of the box.
1. Close up the box using the M3 screws and nuts.

#### Turning it on

1. Connect the 5.5x2.1mm plug from the power converter box to the socket.
1. Plug an IEC cable into the power converter box.
1. Connect the motor control wires on the ribbon cable to the motor and the sensor board wires to the sensor.
1. **TRIPLE CHECK ALL CONNECTIONS, ESPECIALLY THOSE HAVING TO DO WITH MAINS POWER!!!**
1. Turn on the power converter box with the switch.
1. Cross fingers and hope for no magic smoke.
1. Upon bootup, the green POWER light will be lit.
1. If all is good, the red ERROR light will not be lit. If it is, connect the USB cable to your computer and check the serial output; it should be running at 115200 baud by default. Check the detailed log messages.

# Assembly Photos

## Frame

![Frame parts, minus the comb for the slip ring](media/clinostat/1500px/20121117-DSCF5323.png){#fig:frame_parts}

![Ball bearings, press-fit in one of the arms](media/clinostat/1500px/20121117-DSCF5312.png){#fig:frame_bearings}

![Small sproket wheel and motor attached to the arm](media/clinostat/1500px/20121117-DSCF5307.png){#fig:frame_motor}

![Inserted M16 bolt](media/clinostat/1500px/20121117-DSCF5301.png){#fig:frame_bolt}

![Big wheel and M300 belt installed](media/clinostat/1500px/20121117-DSCF5299.png){#fig:frame_wheel}

![Slip ring wheel installed](media/clinostat/1500px/20121117-DSCF5293.png){#fig:frame_slip_ring}

![Second arm installed](media/clinostat/1500px/20121117-DSCF5289.png){#fig:frame_second_arm}

![Completely assembled frame, with M16 nut](media/clinostat/1500px/20121117-DSCF5287.png){#fig:frame_assembled}

## Control boxes

![IEC plug wiring](media/clinostat/1500px/20121117-DSCF5333.png){#fig:iec_wiring}

![5.5x2.1mm plug wiring](media/clinostat/1500px/20121117-DSCF5348.png){#fig:plug_wiring}

![Microstepping control board wiring](media/clinostat/1500px/20121117-DSCF5343.png){#fig:microstepping_wiring}

![Peripherals wiring](media/clinostat/1500px/20121117-DSCF5335.png){#fig:peripherals_wiring}

![Step-down board output wiring](media/clinostat/1500px/20121117-DSCF5353.png){#fig:stepdown_wiring}


# Future & Places for Improvement

![Clinostat installed in the incubator](media/clinostat_incubator.png){#fig:clinostat_incubator}

Due to time limitations with the residency, we were not able to test the clinostat with live cells. We have been able to test however that it properly suspends a sphere of growth medium in simulated microgravity^[For a protocol for culturing mammalian cells in microgravity using a RWV like this one, see [@Zhang2017b].]. So future testing needs to be done using live cells.

The materials for the vessel may need to be changed. The machining for the glass top was done using a waterjet cutter, which caused chipping in the material. It would be best to try and find a form of polycarbonate that could be CNC machined but that is also capable of being autoclaved. The teflon base, plug, and valves required quite precise hand-machining; perhaps it could be re-designed to be done using CNC machining. Total cost for labor and materials for one vessel was around €300 in Slovenia.

The slip ring needs to be redesigned to be more robust and functional. The main issue is that the aluminum combs can't make constant contact with the copper tape on the wheel, causing interruptions in the signals. Perhaps a more secure solution can be found by forming some metal strips securely over the wheel, tied down to both sides of the acrylic base.

In any event, we hope that the reverse-engineering of the vessel itself might allow others to more cheaply have access not only to microgravity experiments, but also 3D tissue culture ones as well.

<!--

# Parts/Components

* [TECNOIOT 5pcs 5A Max XL4015 DC to DC CC CV Step-Down Lithium Battery Charger Converter](https://www.amazon.de/gp/product/B084BVKGDK/ref=ppx_yo_dt_b_asin_title_o06_s01?ie=UTF8&psc=1)
* [Usongshine Stepper Motor Nema 17 1.5A 1.8°4 Lead for 3D Printer](https://www.amazon.de/gp/product/B07MT7VH8Y/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1)
* [QLOUNI Set of 3 Cold Appliances Installation Plug 220-250 V / 10 A Audio Cold Appliance Socket Installation Coupling Including Fuse Cold Appliance Socket C14 with Switch](https://www.amazon.de/-/en/gp/product/B07T5D39R3/ref=ppx_od_dt_b_asin_title_s01?ie=UTF8&psc=1)
* [Hose Pump, Dosing Pump, Hose Pump, Dosing Head, DC 6V 0-150 ml/min, for Chemicals, Liquids and Other Dosing Additives](https://www.amazon.de/gp/product/B07YWHD529/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1)

-->

# Support

Thanks to Lovrenc Košenina for his extensive design, fabrication, and electronics assistance, Kristijan Tkalec for his expert scientific assistance, and Meta Petrič for her hands-on assistance. Special thanks to Špela Petrič.

Created during a Biofriction residency.

Produced by Galerija Kapelica/Zavod Kersnikova in Ljubljana, Slovenia.

Biofriction is supported by the European CommissionCreative Europe.


# References
